# Stears Data Engineer Coding Homework
Thanks for your application to Stears.

This exercise is designed to assess your proficiency with Python, SQL, git, and cloud services. We do not require that you complete all tasks, but we ask that you attempt as much as you can, so we will have enough information to assess your current experience.

### Deadline
You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

### Assessment Criteria
Our favourite submissions are -
1. **Clean & simple code** - Minimal, high quality code that achieves the desired outcome.
2. **Professional & reproducible** - Complete solutions with well explained steps and assumptions. Someone else should be able to run your code and reproduce the exact same result.
3. **Rigorous about data quality** - Solutions have been vetted by data quality checks and perform data cleanup as needed

### Conduct
By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.


## Coding Homework 

### Guidelines

- Every step of your work must be documented in code and entirely reproducible.
- Use Python 3 in a Jupyter Notebook to accomplish this assignment.
- Include a README.md telling us how to use your code. It should include clear setup instructions (assume no knowledge of the submission or the stack) and document any important assumptions.

### Scenario

Our Data Production team has recently received some interesting new household survey data in a CSV file. The survey was given to a sample of households in Nigeria. The sample is comprised of 1,200 households geographically distributed across the country. Each row represents a single household and its responses to survey questions. We want to create a table that summarizes the survey responses in each state.

##### Task 1: Python data wrangling

The raw household survey data is available here: [household-survey-data.csv](https://gitlab.com/stears-interviews/data-engineer/-/blob/develop/household-survey-data.csv)

Your first task is to aggregate the data at the state level to create the desired table output shown below. The x's should be filled in with the real numeric values.

| State | Question | Percent of Households that Answered Yes | Percent of Households that Answered No | Sample Size |
| - | - | - | - | - |
| Abia State | Do you have a mobile money account? | x | x | x |
| Abia State | Do you have an account at a bank or another type of formal financial institution? (Not mobile money)	 | x | x | x |
| Abia State | Do you participate in an informal savings group or investment group (like [insert local example]), or does another person outside the family keep money safe for you, handle your investments or provide you with loans? | x | x | x |
| Adamawa State | Do you have a mobile money account? | x | x | x |
| Adamawa State | Do you have an account at a bank or another type of formal financial institution? (Not mobile money)	 | x | x | x |
| Adamawa State | Do you participate in an informal savings group or investment group (like [insert local example]), or does another person outside the family keep money safe for you, handle your investments or provide you with loans? | x | x | x |
| ... | ... | ... | ... | ... |

Once you have created the table described above, check the table for data quality issues, and fix them as needed.

Export the table as a CSV file called `state_survey_summary.csv`

##### Task 2: SQL database creation and querying

Use the Python `sqlite3` library to create a database. Create two tables in the database:
1. The `state_survey_summary` table from Task 1
2. A lookup table of all the states in each region in Nigeria, provided in this CSV: [region_state_lookup.csv](https://gitlab.com/stears-interviews/data-engineer/-/blob/develop/region_state_lookup.csv)

Once you have created these tables in the database, join them to get the total count of survey respondents per region. 

Export your query result as a CSV called `sample_size_per_region.csv`

### Submission

* Create a private GitLab repository and add the following people as maintainers:
  - @foluso_ogunlana
  - @hannahbkates
  - @abdulrahima
  - @jaymykels69
- Push all your work to your GitLab repository. It should contain the four files you have created:
  - README.md
  - Your ipynb
  - state_survey_summary.csv
  - sample_size_per_region.csv
